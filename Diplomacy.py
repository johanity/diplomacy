#!/usr/bin/env python3
#Johan David Bonilla
# Must War Exist? 




class Army:
    def __init__(self, identification, city):
        '''
        Creates an army.
        '''
        self.dead =  False
        self.identification = identification
        self.city = city
        self.supported = [] 
        self.supporting = [] 
        
    
    def move(self, city):
        '''
        Moves army to given city
        '''
        self.city = city
    
    def support(self, army):
        '''
        Provides support to an army.
        '''
        self.supporting.append(army)
        army.supported.append(self)
    
    def die(self):

        '''
        Kills said army.
        '''
        self.dead = True

    def no_allies(self):
        '''
        Removes all support to any army.
        '''
        if self.supporting:
            for army in self.supporting:
                army.supported.remove(self)
            self.supporting = []

class City:
    def __init__(self, name):
        '''
        Makes a city
        '''
        self.name = name
        self.armies = [] 
    
    def new_army(self, army):
        '''
        Adds an army to the city.
        army: Army object
        '''
        self.armies.append(army)

    def end(self):
        '''
        Kills all armies in the city.
        '''
        for army in self.armies:
            army.die()

class Warzone:
    def __init__(self):
        '''
        Creates a warzone.
        '''
        self.armies = {}
        self.cities = {}

    def read(self, line):
        '''
        Creates a tuple out of a read line using .split()
        '''
        return tuple([i for i in line.split()])

    def refresh(self, actions):
        '''
        Updates the war based on given actions
        '''
        
        if len(actions) == 3:
            self.armies[actions[0]] = Army(actions[0], actions[1])
            if actions[1] not in self.cities:
                self.cities[actions[1]] = City(actions[1])
            self.cities[actions[1]].new_army(self.armies[actions[0]])

        
        elif actions[2] == 'Support':
            self.armies[actions[0]] = Army(actions[0], actions[1])
            self.armies[actions[0]].support(self.armies[actions[3]])
            if actions[1] not in self.cities:
                self.cities[actions[1]] = City(actions[1])
            self.cities[actions[1]].new_army(self.armies[actions[0]])

        
        elif actions[2] == 'Move':
            self.armies[actions[0]] = Army(actions[0], actions[1])
            self.armies[actions[0]].move(actions[3])
            if actions[3] not in self.cities:
                self.cities[actions[3]] = City(actions[3])
            self.cities[actions[3]].new_army(self.armies[actions[0]])

    def print(self, w):
        '''
        Prints the current battle .
        w: output stream
        '''
        for thing in self.state():
            w.write(f'{thing[0]} {thing[1]}\n')
    
    def war(self, city):
        '''
        Simulates a war between armies in a city.
        '''
        support_counts = [len(army.supported) for army in city.armies]
        if support_counts.count(max(support_counts)) > 1:
            city.end()
        else:
            for army in city.armies:
                if len(army.supported) < max(support_counts):
                    army.die()

    def state(self):
        '''
        Returns the current state of the war.
        '''
        
        for i, city in self.cities.items():
            if len(city.armies) > 1:
                for army in city.armies:
                    army.no_allies()

        
        for i, city in self.cities.items():
            if len(city.armies) > 1:
                self.war(city)
                
        return [(answer.identification, '[dead]' if answer.dead else answer.city) for i, answer in self.armies.items()]
        

def diplomacy_solve(r, w):
    """
    r a reader
    w a writer
    """
    war = Warzone()
    for line in r:
        res = war.read(line)
        war.refresh(res)
    war.print(w)